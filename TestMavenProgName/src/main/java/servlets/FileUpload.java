package servlets;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

@WebServlet("/")
public class FileUpload extends HttpServlet{

    private static final Logger logger = Logger.getLogger(FileUpload.class);

    public static Properties getEnvVars() throws Throwable {
        Process p = null;
        Properties envVars = new Properties();
        Runtime r = Runtime.getRuntime();
        String OS = System.getProperty("os.name").toLowerCase();
        if (OS.indexOf("windows 9") > -1) {
            p = r.exec( "command.com /c set" );
        }
        else if ( (OS.indexOf("nt") > -1)
                || (OS.indexOf("windows 2000") > -1 )
                || (OS.indexOf("windows xp") > -1)
                || (OS.indexOf("windows 8.1") > -1)) {
            p = r.exec( "cmd.exe /c set" );
        }
        else {
            p = r.exec( "env" );
        }
        BufferedReader br = new BufferedReader
                ( new InputStreamReader( p.getInputStream() ) );
        String line;
        while( (line = br.readLine()) != null ) {
            int idx = line.indexOf( '=' );
            String key = line.substring( 0, idx );
            String value = line.substring( idx+1 );
            envVars.setProperty( key, value );
        }
        return envVars;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        logger.info("Запуск метода");
        try {
            Properties p = FileUpload.getEnvVars();
            String PATH_TO_PROPERTIES = p.getProperty("FileUploadConfig");
            logger.info("PATH_TO_PROPERTIES is find");
            Properties prop = new Properties();
            FileInputStream fis;

            try {
                fis = new FileInputStream(PATH_TO_PROPERTIES);
                prop.load(fis);
                String value = prop.getProperty("PATH_UPLOADFILE");
                DiskFileItemFactory dfif = new DiskFileItemFactory();
                dfif.setSizeThreshold(1024*1024);
                dfif.setRepository(new File(value));
                ServletFileUpload sfu = new ServletFileUpload(dfif);
                sfu.setFileSizeMax(1024*1024);

                try{
                    List item = sfu.parseRequest(request);
                    Iterator iterator = item.iterator();
                    while(iterator.hasNext()){
                        FileItem fileItem = (FileItem)iterator.next();
                        String fileName = "";
                        if (fileItem.getName().lastIndexOf("\\") != -1) {
                            fileName = fileItem.getName().substring(fileItem.getName().lastIndexOf("\\") + 1, fileItem.getName().length());
                        } else {
                            fileName = fileItem.getName();
                        }
                        File file = new File(value, fileName);
                        fileItem.write(file);
                    }
                } catch (Exception e){
                    logger.info("Мы что-то поймали! ", e);
                }
            } catch (IOException e) {
                logger.info("File " + PATH_TO_PROPERTIES + " is missing ", e);
            }


        }
        catch (Throwable e) {
            logger.info("PATH_TO_PROPERTIES not find ", e);
        }



        request.getRequestDispatcher("/second.jsp").forward(request, response);
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        processRequest(request,response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        processRequest(request, response);
    }
}
